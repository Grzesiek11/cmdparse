# cmdparse

A simple Rust-based command parser. It's not meant to "do everything" like most ones out there, it focuses on just parsing. The program (or another library) itself should take care of the error handling, help system etc.

Made for and split from [the El project](https://gitlab.com/Grzesiek11/El), but can be used for any project that requires command parsing.

## License

cmdparse is available under the terms of the ISC License. See [LICENSE](LICENSE).
