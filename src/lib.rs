/*
 * This file is a part of cmdparse: <https://gitlab.com/Grzesiek11/cmdparse>
 * cmdparse is available under the terms of the ISC License.
 * The license is distributed with the project, but if you don't have a copy, see <https://opensource.org/licenses/isc-license> instead.
 */

use std::{collections::HashMap, any::Any};
use std::result::Result as StdResult;

pub mod parser;

#[derive(Debug)]
pub enum Error {
    UnclosedScope(parser::QuoteScope),
    UnknownCommand(Vec<String>),
    TypeMismatch(String, Value, String),
    NoValueProvided(String, Value),
    ArgumentRepeated(String),
    UnknownArgument(String),
    ArgumentNotProvided(String),
    NonStringArgumentJoined,
    BadConversion,
    NoValue,
}

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug, Clone)]
pub enum ParsedValue {
    None,
    String(String),
    I32(i32),
    U32(u32),
    I64(i64),
    U64(u64),
    F64(f64),
}

impl ParsedValue {
    pub fn as_str(&self) -> Result<&str> {
        if let ParsedValue::String(string) = self {
            Ok(string)
        } else {
            Err(Error::BadConversion)
        }
    }

    pub fn as_i32(&self) -> Result<i32> {
        if let ParsedValue::I32(int) = self {
            Ok(*int)
        } else {
            Err(Error::BadConversion)
        }
    }

    pub fn as_u32(&self) -> Result<u32> {
        if let ParsedValue::U32(int) = self {
            Ok(*int)
        } else {
            Err(Error::BadConversion)
        }
    }

    pub fn as_i64(&self) -> Result<i64> {
        if let ParsedValue::I64(int) = self {
            Ok(*int)
        } else {
            Err(Error::BadConversion)
        }
    }

    pub fn as_u64(&self) -> Result<u64> {
        if let ParsedValue::U64(int) = self {
            Ok(*int)
        } else {
            Err(Error::BadConversion)
        }
    }
}

#[derive(Debug)]
pub struct ParsedArgument {
    pub values: Vec<ParsedValue>,
}

impl ParsedArgument {
    pub fn try_value(&self) -> Option<&ParsedValue> {
        self.values.get(0)
    }

    pub fn value(&self) -> Result<&ParsedValue> {
        self.values.get(0).ok_or_else(|| Error::NoValue)
    }
}

#[derive(Debug, Clone)]
pub enum Value {
    None,
    String,
    I32,
    U32,
    I64,
    U64,
    F64,
}

#[derive(Debug)]
pub enum ArgumentKind {
    Positional(u8),
    Option { long: Option<String>, short: Option<char> },
}

#[derive(Debug)]
pub enum Repeat {
    True,
    False,
    Join,
}

#[derive(Debug)]
pub struct Argument {
    pub kind: ArgumentKind,
    pub value: Value,
    pub default: Option<ParsedValue>,
    pub required: bool,
    pub repeating: Repeat,
}

pub struct Command {
    pub subcommands: HashMap<String, Node>,
    pub arguments: HashMap<String, Argument>,
    pub parse_negative_numbers: bool,
    pub metadata: Box<dyn Any + Send + Sync>,
}

pub enum Node {
    Command(Command),
    Alias(Vec<String>),
}

pub enum Input {
    Command(Command),
    Tree(HashMap<String, Node>),
}

pub fn parse_parts<'a>(parts: &[String], input: &'a Input) -> Result<(&'a Command, HashMap<String, ParsedArgument>, Vec<String>)> {
    // Find command and arguments
    let (command, command_path, arguments) = parser::get_command_and_arguments(parts.to_vec(), input)?;
    // Parse arguments
    let parsed_arguments = parser::parse_arguments(&command, &arguments)?;

    Ok((command, parsed_arguments, command_path))
}

pub fn parse_string<'a, 'b>(command_string: &str, input: &'a Input, prefixes: &'b [String]) -> Result<Option<(&'a Command, HashMap<String, ParsedArgument>, Vec<String>, &'b str)>> {
    // Find and strip prefix
    if let Some((prefix, command_string)) = parser::strip_prefix(&prefixes, &command_string) {
        // Split string into parts
        let parts = parser::split_command_string(command_string)?;
        // Parse the parts into command and arguments
        let (command, arguments, command_path) = parse_parts(&parts, input)?;

        Ok(Some((command, arguments, command_path, prefix)))
    } else {
        Ok(None)
    }
}
