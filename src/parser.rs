/*
 * This file is a part of cmdparse: <https://gitlab.com/Grzesiek11/cmdparse>
 * cmdparse is available under the terms of the ISC License.
 * The license is distributed with the project, but if you don't have a copy, see <https://opensource.org/licenses/isc-license> instead.
 */

use std::collections::HashMap;

use crate::{Error, Input, Command, ParsedArgument, Value, ParsedValue, Argument, ArgumentKind, Repeat, Node, Result};

impl Repeat {
    fn is_repeating(&self) -> bool {
        match self {
            Repeat::True => true,
            _ => false,
        }
    }

    fn is_joining(&self) -> bool {
        match self {
            Repeat::Join => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub enum QuoteScope {
    Normal,
    SingleQuotes,
    DoubleQuotes,
}

impl QuoteScope {
    fn is_normal(&self) -> bool {
        match self {
            QuoteScope::Normal => true,
            _ => false,
        }
    }
}

pub fn strip_prefix<'a, 'b>(prefixes: &'a [String], string: &'b str) -> Option<(&'a str, &'b str)> {
    for prefix in prefixes {
        if let Some(stripped) = string.strip_prefix(prefix) {
            return Some((prefix, stripped));
        }
    }

    None
}

pub fn split_command_string(string: &str) -> Result<Vec<String>> {
    let mut parts = Vec::new();

    let mut scope = QuoteScope::Normal;
    let mut part = String::new();
    let mut string_iter = string.chars().peekable();
    while let Some(ch) = string_iter.next() {
        let c = match ch {
            ' ' => match scope {
                QuoteScope::Normal => {
                    parts.push(part);
                    part = String::new();
                    None
                },
                _ => Some(' '),
            },
            '"' => match scope {
                QuoteScope::Normal => { scope = QuoteScope::DoubleQuotes; None },
                QuoteScope::SingleQuotes => Some('"'),
                QuoteScope::DoubleQuotes => { scope = QuoteScope::Normal; None },
            },
            '\'' => match scope {
                QuoteScope::Normal => { scope = QuoteScope::SingleQuotes; None },
                QuoteScope::SingleQuotes => { scope = QuoteScope::Normal; None },
                QuoteScope::DoubleQuotes => Some('\''),
            },
            '\\' => {
                let next_ch = string_iter.peek();
                match next_ch {
                    Some(next_ch) if ['\\', '"', '\'', ' ', 'n'].contains(next_ch) => {
                        let ch = match next_ch {
                            '\\' => Some('\\'),
                            '"' => Some('"'),
                            '\'' => Some('\''),
                            ' ' => Some(' '),
                            'n' => Some('\n'),
                            _ => None,
                        };
                        string_iter.next();
                        ch
                    },
                    _ => Some('\\'),
                }
            },
            ch => Some(ch),
        };

        if let Some(c) = c {
            part.push(c);
        }
    }

    if !scope.is_normal() {
        return Err(Error::UnclosedScope(scope));
    }

    parts.push(part);

    Ok(parts)
}

pub fn get_command_and_arguments<'a>(mut parts: Vec<String>, input: &'a Input) -> Result<(&'a Command, Vec<String>, Vec<String>)> {
    loop {
        let mut path = Vec::new();
        let mut current_input = match input {
            Input::Tree(subcommands) => subcommands,
            Input::Command(command) => return Ok((command, path, parts)),
        };
        let mut unalias = None;
        let mut found_command = None;

        for (i, part) in parts.iter().enumerate() {
            let mut path_end = true;
            match current_input.get(part) {
                Some(Node::Alias(alias)) => {
                    unalias = Some((i, alias.to_owned()));
                    break;
                },
                Some(Node::Command(command)) => {
                    current_input = &command.subcommands;
                    path.push(part.to_owned());
                    path_end = false;
                    found_command = Some((command, i));
                },
                None => (),
            }

            if let Some((command, argument_index)) = found_command {
                if i + 1 == parts.len() || current_input.is_empty() || path_end {
                    let mut arguments = parts;
                    arguments.drain(0..argument_index + 1);

                    return Ok((&command, path, arguments));
                }
            } else {
                return Err(Error::UnknownCommand(path));
            }
        }

        if let Some((index, alias)) = unalias {
            parts.splice(0..index + 1, alias);
        } else {
            return Err(Error::UnknownCommand(path));
        }
    }
}

pub fn parse_arguments(command: &Command, arguments: &[String]) -> Result<HashMap<String, ParsedArgument>> {
    enum ArgumentScope {
        Normal,
        Positional,
    }

    enum ExpectedType<'a> {
        Positional(&'a str),
        ShortOption(&'a str),
        LongOption(&'a str, Option<&'a str>),
    }

    fn parse_value(value: String, value_type: &Value, argument_id: &str) -> Result<ParsedValue> {
        let map_error = || Error::TypeMismatch(argument_id.to_string(), value_type.clone(), value.to_owned());
        let map_int_error = |_| map_error();
        let map_float_error = |_| map_error();

        match value_type {
            Value::String => Ok(ParsedValue::String(value)),
            Value::I32 => Ok(ParsedValue::I32(value.parse().map_err(map_int_error)?)),
            Value::U32 => Ok(ParsedValue::U32(value.parse().map_err(map_int_error)?)),
            Value::I64 => Ok(ParsedValue::I64(value.parse().map_err(map_int_error)?)),
            Value::U64 => Ok(ParsedValue::U64(value.parse().map_err(map_int_error)?)),
            Value::F64 => Ok(ParsedValue::F64(value.parse().map_err(map_float_error)?)),
            Value::None => Ok(ParsedValue::None),
        }
    }

    fn match_positional(positional: &str, positional_counter: &mut u8, position: u8, command_argument: &Argument, argument_id: &str) -> Result<Vec<ParsedValue>> {
        let mut values = Vec::new();

        if *positional_counter == position {
            if !(command_argument.repeating.is_repeating() || command_argument.repeating.is_joining()) {
                *positional_counter += 1;
            }
            values.push(parse_value(positional.to_string(), &command_argument.value, argument_id)?);
        }

        Ok(values)
    }

    fn match_long_option(option: &str, raw_value: Option<&str>, expected_option: &str, expected_value: &Value, arguments_iter: &mut dyn Iterator<Item = &String>, argument_id: &str) -> Result<Vec<ParsedValue>> {
        let mut values = Vec::new();

        if option == expected_option {
            let raw_value = match raw_value {
                Some(val) => Some(val),
                None => match arguments_iter.next() {
                    Some(next_arg) => Some(next_arg.as_str()),
                    None => None,
                },
            };

            values.push(match expected_value {
                Value::None => ParsedValue::None,
                _ => match raw_value {
                    Some(raw_value) => parse_value(raw_value.to_owned(), expected_value, argument_id)?,
                    None => return Err(Error::NoValueProvided(argument_id.to_string(), expected_value.clone())),
                },
            });
        }

        Ok(values)
    }

    // TODO: This could be optimized by parsing entire argument groups at once
    fn match_short_options(options: &str, expected_option: char, expected_value: &Value, arguments_iter: &mut dyn Iterator<Item = &String>, argument_id: &str) -> Result<Vec<ParsedValue>> {
        let mut values = Vec::new();

        let mut options_iter = options.chars().peekable();
        while let Some(short_option) = options_iter.next() {
            if short_option == expected_option {
                values.push(match expected_value {
                    Value::None => ParsedValue::None,
                    _ => {
                        // Error if short option with value is used in the middle of short option string
                        if options_iter.peek().is_some() {
                            return Err(Error::NoValueProvided(String::new(), expected_value.clone()));
                        }

                        if let Some(next_argument) = arguments_iter.next() {
                            parse_value(next_argument.to_owned(), expected_value, argument_id)?
                        } else {
                            return Err(Error::NoValueProvided(String::new(), expected_value.clone()));
                        }
                    },
                });
            }
        }

        Ok(values)
    }

    let mut parsed_arguments = HashMap::new();

    let mut scope = ArgumentScope::Normal;
    let mut arguments_iter = arguments.iter().peekable();
    let mut positional_counter: u8 = 0;
    while let Some(argument) = arguments_iter.next() {
        // Get what type of argument is expected based on prefix & scope
        let expected_type = match scope {
            ArgumentScope::Normal => {
                if let Some(long_option) = argument.strip_prefix("--") {
                    // Handle `--`
                    if long_option.is_empty() {
                        scope = ArgumentScope::Positional;
                        continue;
                    }

                    // Handle `--option=value` syntax
                    let (long_option, raw_value) = match long_option.split_once('=') {
                        Some((opt, val)) => (opt, Some(val)),
                        None => (long_option, None),
                    };

                    ExpectedType::LongOption(long_option, raw_value)
                } else if let Some(short_options) = argument.strip_prefix("-") {
                    // Handle negative numbers
                    let mut is_negative_number = false;
                    if command.parse_negative_numbers {
                        is_negative_number = true;
                        for c in short_options.chars() {
                            if !c.is_numeric() {
                                is_negative_number = false;
                                break;
                            }
                        }
                    }

                    // Also handle `-`
                    if is_negative_number || short_options.is_empty() {
                        ExpectedType::Positional(argument)
                    } else {
                        ExpectedType::ShortOption(short_options)
                    }
                } else {
                    ExpectedType::Positional(argument)
                }
            },
            ArgumentScope::Positional => ExpectedType::Positional(argument),
        };

        let mut found_argument = false;
        // Find the matching command argument
        for (argument_id, command_argument) in &command.arguments {
            // Match what is expected against the argument
            let values = match (&expected_type, &command_argument.kind) {
                (ExpectedType::Positional(positional), ArgumentKind::Positional(position))
                    => match_positional(positional, &mut positional_counter, *position, command_argument, &argument_id)?,
                (ExpectedType::LongOption(option, raw_value), ArgumentKind::Option { long: Some(expected_option), .. })
                    => match_long_option(option, *raw_value, expected_option, &command_argument.value, &mut arguments_iter, &argument_id)?,
                (ExpectedType::ShortOption(options), ArgumentKind::Option { short: Some(expected_option), .. })
                    => match_short_options(options, *expected_option, &command_argument.value, &mut arguments_iter, &argument_id)?,
                _ => Vec::new(),
            };

            // Check if that was the correct argument
            if !values.is_empty() {
                let parsed_argument = parsed_arguments.entry(argument_id.to_owned()).or_insert(ParsedArgument { values: Vec::new() });
                for value in values {
                    // Check if it was not repeated when it shouldn't be
                    if parsed_argument.values.len() == 0 || command_argument.repeating.is_repeating() {
                        parsed_argument.values.push(value);
                    } else if command_argument.repeating.is_joining() {
                        if let (Some(ParsedValue::String(string_value)), ParsedValue::String(new_value)) = (parsed_argument.values.get_mut(0), value) {
                            string_value.push(' ');
                            string_value.push_str(&new_value);
                        } else {
                            return Err(Error::NonStringArgumentJoined);
                        }
                    } else {
                        return Err(Error::ArgumentRepeated(argument_id.to_owned()));
                    }
                }

                found_argument = true;
                // There is only one correct argument (assuming it's not a short option), so break out of the checking loop here
                match expected_type {
                    ExpectedType::ShortOption(_) => (),
                    _ => break,
                }
            }
        }

        // If there is no matching argument, the input is incorrect
        if !found_argument {
            return Err(Error::UnknownArgument(argument.to_owned()));
        }
    }

    // Check for missing arguments
    for (arg_id, command_argument) in &command.arguments {
        if !parsed_arguments.contains_key(arg_id) {
            if command_argument.required {
                // Error on missing required arguments
                return Err(Error::ArgumentNotProvided(arg_id.to_owned()));
            } else {
                // Insert empty missing non-required arguments
                let parsed_arg = parsed_arguments.entry(arg_id.to_owned()).or_insert(ParsedArgument { values: Vec::new() });

                // Add default on missing aruments with defaults
                if let Some(default) = &command_argument.default {
                    parsed_arg.values.push(default.clone());
                }
            }
        }
    }

    Ok(parsed_arguments)
}
